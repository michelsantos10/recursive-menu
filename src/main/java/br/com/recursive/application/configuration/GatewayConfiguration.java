package br.com.recursive.application.configuration;

import br.com.recursive.application.core.gateway.GetMenuGateway;
import br.com.recursive.application.dataproviders.memory.MenuMemoryDataProvider;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class GatewayConfiguration {
    @Bean
    public GetMenuGateway getMenuGateway() {
        return new MenuMemoryDataProvider();
    }
}
