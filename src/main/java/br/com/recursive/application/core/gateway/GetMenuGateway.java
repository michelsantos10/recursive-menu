package br.com.recursive.application.core.gateway;

import br.com.recursive.application.core.model.RecursiveMenuModel;

public interface GetMenuGateway {
    public RecursiveMenuModel getAllMenu();
}
