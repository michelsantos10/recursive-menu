package br.com.recursive.application.core.usecase;

import br.com.recursive.application.core.gateway.GetMenuGateway;
import br.com.recursive.application.core.model.RecursiveMenuModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class GetMenuTreeUseCase {
    @Autowired
    GetMenuGateway getMenuGateway;

    public RecursiveMenuModel getAllMenuTree() {
        return this.getMenuGateway.getAllMenu();
    }
}
