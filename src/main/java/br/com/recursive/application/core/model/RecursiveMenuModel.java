package br.com.recursive.application.core.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class RecursiveMenuModel {
    private Long id;
    private String name;
    private RecursiveMenuModel parent;
    private List<RecursiveMenuModel> children;
    private Set<Long> addedChildren;

    public RecursiveMenuModel() {
        this.children = new ArrayList<>();
        this.addedChildren = new HashSet<>();
    }

    public RecursiveMenuModel(Long id, String name) {
        this();
        this.id = id;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public RecursiveMenuModel getParent() {
        return parent;
    }

    public void setParent(RecursiveMenuModel parent, boolean canAddThisIntoChild) {
        this.parent = parent;
        if (canAddThisIntoChild) {
            this.parent.addChild(this);
        }
    }

    public List<RecursiveMenuModel> getChildren() {
        return children;
    }

    public void addChild(RecursiveMenuModel children) {
        if (children == null) {
            throw new IllegalArgumentException("\"children\" parameter cannot be null.");
        }
        if (this.addedChildren.contains(children.getId())) {
            throw new IllegalArgumentException("The value (id:" + children.getId() + " name:" + children.getName() + ")  is already in the children.");
        }
        this.addedChildren.add(children.getId());
        this.children.add(children);
    }
}
