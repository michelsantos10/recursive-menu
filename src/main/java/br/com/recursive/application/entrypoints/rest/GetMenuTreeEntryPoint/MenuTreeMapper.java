package br.com.recursive.application.entrypoints.rest.GetMenuTreeEntryPoint;

import br.com.recursive.application.core.model.RecursiveMenuModel;

import java.util.List;

public class MenuTreeMapper {
    public MenuTreeModel fromRecursiveMenuModel(RecursiveMenuModel recursiveMenuModel) {
        MenuTreeModel menuTreeModel = new MenuTreeModel();
        if (recursiveMenuModel.getChildren().size() > 0) {
            this.fromRecursiveMenuModel(recursiveMenuModel.getChildren(), menuTreeModel);
        } else {
            throw new IllegalArgumentException("Menu should be 1 or more children.");
        }
        return menuTreeModel;
    }

    private void fromRecursiveMenuModel(List<RecursiveMenuModel> menus, MenuTreeModel menuTreeModelWriter) {
        for (RecursiveMenuModel menu : menus) {
            MenuTreeModel menuChild = new MenuTreeModel(menu.getName(),null,"#", false,false);
            menuTreeModelWriter.getChildren().add(menuChild);
            if (menu.getChildren().size() > 0) {
                this.fromRecursiveMenuModel(menu.getChildren(), menuChild);
            }
        }
    }
}
