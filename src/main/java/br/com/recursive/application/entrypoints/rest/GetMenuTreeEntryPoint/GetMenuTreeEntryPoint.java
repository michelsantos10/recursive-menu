package br.com.recursive.application.entrypoints.rest.GetMenuTreeEntryPoint;

import br.com.recursive.application.core.usecase.GetMenuTreeUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GetMenuTreeEntryPoint {

    @Autowired
    GetMenuTreeUseCase getMenuTreeUseCase;

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping(path = "/menu")
    public ResponseEntity<?> GetAllMenuTree() {
        MenuTreeMapper menuTreeMapper = new MenuTreeMapper();
        MenuTreeModel menuTree = menuTreeMapper.fromRecursiveMenuModel(getMenuTreeUseCase.getAllMenuTree());
        return new ResponseEntity<>(menuTree.getChildren(), HttpStatus.OK);
    }
}
