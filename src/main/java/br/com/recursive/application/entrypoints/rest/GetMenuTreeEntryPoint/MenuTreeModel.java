package br.com.recursive.application.entrypoints.rest.GetMenuTreeEntryPoint;

import java.util.ArrayList;
import java.util.List;

public class MenuTreeModel {
    private String title;
    private String icon;
    private String link;
    private Boolean home;
    private Boolean group;
    private List<MenuTreeModel> children;

    public MenuTreeModel() {
        this.children = new ArrayList<>();
        this.home = false;
        this.group = false;
    }

    public MenuTreeModel(String title, String icon, String link, Boolean home, Boolean group) {
        this();
        this.title = title;
        this.icon = icon;
        this.link = link;
        this.home = home;
        this.group = group;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public Boolean getHome() {
        return home;
    }

    public void setHome(Boolean home) {
        this.home = home;
    }

    public Boolean getGroup() {
        return group;
    }

    public void setGroup(Boolean group) {
        this.group = group;
    }

    public List<MenuTreeModel> getChildren() {
        return children;
    }
}
