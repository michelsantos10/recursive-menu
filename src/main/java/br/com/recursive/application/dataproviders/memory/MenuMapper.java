package br.com.recursive.application.dataproviders.memory;

import br.com.recursive.application.core.model.RecursiveMenuModel;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class MenuMapper {
    public RecursiveMenuModel menuTableToRecursiveMenu(List<MenuTableModel> menuTableModel) {
        Map<Long, MenuTableModel> indexedMenuTable = menuTableModel.stream().collect(Collectors.toMap(MenuTableModel::getId, o -> o));
        Map<Long, RecursiveMenuModel> indexedRecursiveMenu = new HashMap<>();
        RecursiveMenuModel recursiveMenuModel = new RecursiveMenuModel(0L, "root");
        indexedRecursiveMenu.put(recursiveMenuModel.getId(), recursiveMenuModel);

        for (MenuTableModel menu : menuTableModel) {
            if (!indexedRecursiveMenu.containsKey(menu.getId())) {
                indexedRecursiveMenu.put(menu.getId(), new RecursiveMenuModel(menu.getId(), menu.getName()));
            }
            if (!indexedRecursiveMenu.containsKey(menu.getParentId())) {
                MenuTableModel menuTableModelParent = indexedMenuTable.get(menu.getParentId());
                indexedRecursiveMenu.put(menuTableModelParent.getId(), new RecursiveMenuModel(menuTableModelParent.getId(), menuTableModelParent.getName()));
            }
            if (menu.getParentId() == 0) {
                recursiveMenuModel.addChild(indexedRecursiveMenu.get(menu.getId()));
            } else {
                indexedRecursiveMenu
                        .get(menu.getId())
                        .setParent(indexedRecursiveMenu.get(menu.getParentId()), true);
            }
        }
        return recursiveMenuModel;
    }
}
